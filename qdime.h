
/*
	March 2015
	Implementing QoS-aware Dime (Q-DIME)
	Dime Implementation Type: Trace Version
	Redundancy Suppression method: Hashtable (default: disabled)
*/

/*
	14 Apr
	Code changes
	1- changed dime_below_threshold() to dime_above_threshold(), and changed the operator
	2- commented code used to test redundancy suppression (Positives, Negatives, etc.).
	3- Saved version number and metric value for each trace to test Q-DIME correctness (commented)	
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <assert.h>
#include <unordered_map>
#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
// Shared memory includes
#include <sys/ipc.h>
#include <sys/shm.h>

// Shared memory variables
int shm_id;
key_t shm_key = 9491;

// metrics
typedef struct Metrics
{
	int var0;

} Metrics;

Metrics* pmetric;
Metrics threshold;

/* threshold knobs */
KNOB<double> KnobThresholdVar0(KNOB_MODE_WRITEONCE, "pintool", "th0", "100", "Threshold for var0");


static ofstream Trace_File2;//for testing data if needed
//static const INT32 MAX_SIZE = 86400;//enough to write a string of 11 char's every second for a full day
//static int Readings[2][MAX_SIZE];//Record for each trace: version and metric value (for testing and to calculate overshoots)
//static INT32 Counter = 0;
static REG Version_Reg;//used by INS_InsertVersionCase()
enum 
{
    VERSION_INSTRUMENT,
    VERSION_BASE
};
//------------------
//Q-DIME
//static INT64 Metric_Threshold;
//static INT64 Metric_Value;
//------------------
//Redundancy Suppression: the log is a hashtable (unordered_map)
// 0: Redundancy Suppression disabled (default)
KNOB<int> KnobRunNum(KNOB_MODE_WRITEONCE, "pintool", "r", "0", "Run Number (for redundancy suppression)");
BOOL Redun_Suppress = false;
int Run_Num = 0;//DIME run
static TLS_KEY Tls_Key;
PIN_LOCK Lock;
INT32 Num_Threads = 0;
//------------------
class ThreadData
{
  public:
    ThreadData() : Previous_Trace(0), Previous_Size(0), Total_Test(0), /*Indx_Pos(0), Indx_Neg(0),*/ Errors("") {}
    std::unordered_map<UINT64,USIZE> Log;//trace relative address, trace size: only for the instrumented traces
    UINT64 Previous_Trace;//relative address of previous trace whose version = 1
    USIZE Previous_Size;//size of previous trace whose version = 1
    int Total_Test;//total number of traces compared to Log
//    UINT64 Positives[10000][2];//for instrumented traces (for testing)
//    int Indx_Pos;//index of Positives
//    UINT64 Negatives[10000][2];//for uninstrumented traces that are found in log (for testing)
//    int Indx_Neg;//index of Negatives
    string Errors;
	FILE* Trace_File;//use it if you need a Trace_File for each thread to avoid racing
};
/* ----------------------------------------------------------------- */
// function to access thread-specific data
ThreadData* get_tls(THREADID thread_id)
{
    ThreadData* tdata = 
          static_cast<ThreadData*>(PIN_GetThreadData(Tls_Key, thread_id));
    return tdata;
}
/* ----------------------------------------------------------------- */
int read_log(string file, THREADID thread_id)
{
	string line;
	ifstream myfile(file);
	int ret = 0;
	UINT64 tr;
	USIZE sz;
	ThreadData* tdata = get_tls(thread_id);
	if (myfile.is_open() && myfile.good())
	{
		while(getline (myfile,line,'\n'))
		{
			istringstream iss;
			iss.str(line);
			if(Run_Num > 1)//log
			{
				iss >> tr >> sz;
				tdata->Log[tr] = sz;
			}
		}
		ret = 1;
	 }
	 myfile.close();	
	 return ret;
}
/* ----------------------------------------------------------------- */
//return 1 if we should disable instrumentation
static inline int dime_break_threshold()
{	//If possible, avoid control flow (if/else) to enable Pin to inline this function
	//Q-DIME
	/* Do not disable instrumentation if metric value = zero.
	   This zero appears due to delay in metric collection or pipe reading.
	   Always keep this condition*/
	return ((pmetric->var0 !=0) && (pmetric->var0 < threshold.var0));
}

/* ================================================================= */
/* ----------------------- Switching Versions ---------------------- */
/*	From Pin Documentation:
	* INS_InsertVersionCase(): Insert a dynamic test to switch between versions before ins. If the value in reg matches case_value, then continue execution at ins with version version. Switching to a new version will cause execution to continue at a new trace starting with ins. This API can be called multiple times for the same instruction, creating a switch/case construct to select the version.
	* By default, all traces have a version value of 0. A trace with version value N only transfers control to successor traces with version value N. There are some situations where Pin will reset the version value to 0, even if executing a non 0 trace. This may occur after a system call, exception or other unusual control flow. 
*/
/* ----------------------------------------------------------------- */
/* TV: Switches version if required */
static inline void dime_switch_version(ADDRINT version, INS ins)
{
	//For previous trace, record version and metric value
//	Readings[0][Counter] = version;
//	Readings[1][Counter] = pmetric->var0;
//	Counter++;
	//------------
	INS_InsertCall(ins, IPOINT_BEFORE, AFUNPTR(dime_break_threshold),
		                   IARG_RETURN_REGS, Version_Reg,IARG_END);
	if(version == VERSION_BASE) {  //check if you need to switch to VERSION_INSTRUMENT
		INS_InsertVersionCase(ins, Version_Reg, 0, VERSION_INSTRUMENT, IARG_END);      
	}
	else if(version == VERSION_INSTRUMENT){  //check if you need to switch to VERSION_BASE
		INS_InsertVersionCase(ins, Version_Reg, 1, VERSION_BASE, IARG_END);      
	}
}

/* ----------------------------------------------------------------- */
static inline bool dime_compare_to_log(THREADID thread_id, UINT64 trace_rel_addr, USIZE trace_size)
{
	bool ret_val = 0;	
	if(Redun_Suppress)
	{	
		std::unordered_map<UINT64,USIZE>::iterator Iterator;
		ThreadData* tdata = get_tls(thread_id);
		if(tdata->Log.empty())
		{
			ret_val = 1;
		}
		else
		{
			tdata->Total_Test++;
			Iterator = tdata->Log.find(trace_rel_addr);
			//avg. case: constant, worst case: linear
			//check keys only (range checking is not possible)
			if(Iterator == tdata->Log.end())//not found
			{
				ret_val = 1;
			}
			else
			{
				//found as key
				ret_val = 0;
	//			tdata->Negatives[tdata->Indx_Neg][0] = trace_rel_addr;
	//			tdata->Negatives[tdata->Indx_Neg][1] = trace_size;
	//			tdata->Indx_Neg++;
			}
		}
	}
	return ret_val;
}
/* ----------------------------------------------------------------- */
static inline void dime_modify_log(ADDRINT version, THREADID thread_id, UINT64 trace_rel_addr, USIZE trace_size)
{
	if(Redun_Suppress)
	{
		std::unordered_map<UINT64,USIZE>::iterator Iterator;
		ThreadData* tdata = get_tls(thread_id);
		USIZE new_size;
		if(version == VERSION_BASE && trace_rel_addr == tdata->Previous_Trace)
		{
			//handle the case in which: the trace initialy has version 1, 
			//then Pin checks budget, accordingly the trace switches to version 0
			//therefore, remove this trace from the log
			if(tdata->Log.erase(trace_rel_addr) != 1)
			{
				ostringstream ss;
				ss <<  "Error " << trace_rel_addr;
				tdata->Errors += ss.str();
				tdata->Errors += " not erased\n";
			}
			tdata->Previous_Trace = 0;
			tdata->Previous_Size = 0;
		}
		else if(version == VERSION_BASE && 
			tdata->Previous_Trace < trace_rel_addr && trace_rel_addr < (tdata->Previous_Trace + tdata->Previous_Size))
		{
			//Adjusting trace size.
			//handle the case in which: trace A is instrumented and saved in the log <A,size(A)
			//In the middle of the trace, the version is switched to version 0. So, modify size(A)
			Iterator = tdata->Log.find(tdata->Previous_Trace);
			//avg. case: constant, worst case: linear
			if(Iterator != tdata->Log.end())
			{
				new_size = trace_rel_addr - tdata->Previous_Trace;
				Iterator-> second = new_size;//modify
				tdata->Previous_Size = new_size;
			}
			else
			{
				ostringstream ss;
				ss << tdata->Previous_Trace << tdata->Previous_Size <<  trace_rel_addr;
				tdata->Errors += ss.str();
				tdata->Errors += " size not modified\n";
			}
		}
		else if(version == VERSION_INSTRUMENT)//record instrumented trace
		{
			tdata->Log[trace_rel_addr] = trace_size;
			//avg. case: constant, worst case: linear
			tdata->Previous_Trace = trace_rel_addr;
			tdata->Previous_Size = trace_size;
		
	//		tdata->Positives[tdata->Indx_Pos][0] = trace_rel_addr;
	//		tdata->Positives[tdata->Indx_Pos][1] = trace_size;
	//		tdata->Indx_Pos++;
		}
	}
}
/* ----------------------------------------------------------------- */
//writes Budget_Array to Trace_File2 (Budget_Array holds the budget values before reset)
//writes Log to logfile
//writes to Run file
static inline void dime_fini()
{
//		Trace_File2 << "/begin Threshold = " << threshold.var0 << endl << flush;
//		for (int i = 0; i < Counter; i++){
//		    Trace_File2 <</* Readings[0][i] <<*/ " " << Readings[1][i] << endl;
//		}

		Trace_File2 << "Threshold = " << threshold.var0 << endl;
		Trace_File2 << "Sh mem (last value) = " << pmetric->var0 << endl;
		Trace_File2.close();
    
    if(Redun_Suppress)
    {
		//log file
		for(int t = 0; t < Num_Threads; t++)
		{
			ostringstream ss;
		 	ss << (t + 1);
		 	string file = "log";
		 	file += ss.str();//thread id
		 	file += ".out";
		 	ofstream logfile;//log file
			logfile.open(file, std::ofstream::out);
			ThreadData* tdata = get_tls(t);
			for ( auto it = tdata->Log.begin(); it != tdata->Log.end(); ++it )
			{
				logfile << it->first << " " << it->second << endl;
			}
			logfile.close();
			//Run File
			//Record positives and negatives to test redundancy suppression
//			ofstream Rfile;
//			ostringstream os;
//		 	os << Run_Num << (t+1);
//			file = "run";
//			file += os.str();
//			file += ".out";
//			Rfile.open (file, std::ofstream::out);		
//			int j;
//			Rfile << "Positives ----\n";
//			for(j = 0; j < tdata->Indx_Pos; j++)
//			{
//				Rfile << tdata->Positives[j][0] << " " << tdata->Positives[j][1] << endl;
//			}
//			Rfile << "Negatives ----\n";
//			for(j = 0; j < tdata->Indx_Neg; j++)
//			{
//				Rfile << tdata->Negatives[j][0] << " " << tdata->Negatives[j][1] << endl;
//			}
//			Rfile << tdata->Errors << endl;
//			Rfile << "Total Comparisons = " << tdata->Total_Test << endl;
//			Rfile.close();
		}
	}
}
/* ----------------------------------------------------------------- */
static inline void dime_thread_start(THREADID thread_id)
{
	PIN_GetLock(&Lock, thread_id+1);
	Num_Threads++;
	PIN_ReleaseLock(&Lock);
	ThreadData* tdata = new ThreadData;
	PIN_SetThreadData(Tls_Key, tdata, thread_id);
	if(Redun_Suppress)
	{		
		//read log file
		if(Run_Num > 1)
		{
			ostringstream os;
			string file = "log";
			os << (thread_id+1);
			file += os.str();//thread id
			file += ".out";
			read_log(file, thread_id);
		}
	}
}
/* ----------------------------------------------------------------- */
/*	Sets Parameters
	Arguments:	filename1: name of Trace_File
				filename2: name of Trace_File2 (to record overshoots for testing)
				threshold: metric threshold for Q-DIME
	Use -1 for defaults						
*/
static inline void dime_init(char *filename2)
{
	/*For testing purposes*/	
	Trace_File2.open (filename2, std::ofstream::out);
    /* Locate shared memory segment */
    if ((shm_id = shmget(shm_key, sizeof(Metrics), 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
    /* Attach shared memory segment to our data space */
    void* attach;
    if ((attach = shmat(shm_id, NULL, SHM_RDONLY)) == (void *) -1) {
        perror("shmat");
        exit(1);
    }
    pmetric = (Metrics*) attach;
    /* Redundancy Suppression */
    if(KnobRunNum.Value() > 0)
    {	
    	Redun_Suppress = true;
    	Run_Num = KnobRunNum.Value();
    }
    /* Thresholds */
    threshold.var0 = KnobThresholdVar0.Value();

    /* For TV & TVC */
	PIN_InitSymbols();
	Version_Reg = PIN_ClaimToolRegister();// Scratch register used to select instrumentation version
	PIN_InitLock(&Lock);
}


