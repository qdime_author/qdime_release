
#include <stdio.h>
#include <string.h>
#include "pin.H"
#include "instlib.H"
#include "portability.H"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <unistd.h> 
#include <time.h>
#include <unordered_set>
#include <set>
#include "qdime.h"

string File_Name = "branch_qdime";//output file name
/* ===================================================================== */
#define sec_to_nsec 1000000000//from second to nanosecond
struct timespec tm;//print timestamp to measure info gain with time
//save unique traces in unordered set; it's faster than set
std::unordered_set<string> Trace_Set;
std::unordered_set<string>::const_iterator Iter_Trace;
//save timestamps in ordered set; I care about the ascending order
std::set<long double> Time_Set;
std::set<long double>::const_iterator Iter_Time;
//Check if the extracted trace is unique (not redundant)
//long double test[10000000];
//int indx = 0;
static inline void CheckIfUnique(string str)
{
	clock_gettime( CLOCK_MONOTONIC, &tm);
//	test[indx] = tm.tv_sec + (long double)tm.tv_nsec/sec_to_nsec;
//	indx++;
	if ( Trace_Set.find(str) == Trace_Set.end())
	{	
		//if not found i.e., new unique trace
		//add the trace to the unordered_set 
		Trace_Set.insert(str);
		//and add the timestamp to the set
		Time_Set.insert(tm.tv_sec + ( double)tm.tv_nsec/sec_to_nsec);
	}
}

/* ===================================================================== */
static char nibble_to_ascii_hex(UINT8 i) {
    if (i<10) return i+'0';
    if (i<16) return i-10+'A';
    return '?';
}

/* ===================================================================== */
static void print_hex_line(char* buf, const UINT8* array, const int length) {
  int n = length;
  int i=0;
  if (length == 0)
      n = XED_MAX_INSTRUCTION_BYTES;
  for( i=0 ; i< n; i++)     {
      buf[2*i+0] = nibble_to_ascii_hex(array[i]>>4);
      buf[2*i+1] = nibble_to_ascii_hex(array[i]&0xF);
  }
  buf[2*i]=0;
}

/* ===================================================================== */
static string
disassemble(UINT64 start, UINT64 stop) {
    UINT64 pc = start;
    xed_state_t dstate;
    xed_syntax_enum_t syntax = XED_SYNTAX_INTEL;
    xed_error_enum_t xed_error;
    xed_decoded_inst_t xedd;
    ostringstream os;
    if (sizeof(ADDRINT) == 4) 
        xed_state_init(&dstate,     
                       XED_MACHINE_MODE_LEGACY_32,
                       XED_ADDRESS_WIDTH_32b, 
                       XED_ADDRESS_WIDTH_32b);
    else
        xed_state_init(&dstate,
                       XED_MACHINE_MODE_LONG_64,
                       XED_ADDRESS_WIDTH_64b, 
                       XED_ADDRESS_WIDTH_64b);

    xed_decoded_inst_zero_set_mode(&xedd, &dstate);
    UINT32 len = 15;
    if (stop - pc < 15)
        len = stop-pc;

    xed_error = xed_decode(&xedd, reinterpret_cast<const UINT8*>(pc), len);
    bool okay = (xed_error == XED_ERROR_NONE);
    iostream::fmtflags fmt = os.flags();
    os << std::setfill('0')
       << std::hex
       << std::setw(sizeof(ADDRINT)*2)
       << pc
       << std::dec
       << " "
       << std::setfill(' ')
       << std::setw(4);

    if (okay) {
        char buffer[200];
        unsigned int dec_len;

        dec_len = xed_decoded_inst_get_length(&xedd);
        print_hex_line(buffer, reinterpret_cast<UINT8*>(pc), dec_len);
        memset(buffer,0,200);
        int dis_okay = xed_format_context(syntax, &xedd, buffer, 200, pc, 0, 0);
        if (dis_okay) 
            os << buffer;
        else
            os << "Error disasassembling pc 0x" << std::hex << pc << std::dec << endl;
        pc += dec_len;
    }
    else { // print the byte and keep going.
        UINT8 memval = *reinterpret_cast<UINT8*>(pc);
        os << "???? " // no extension
           << std::hex
           << std::setw(2)
           << std::setfill('0')
           << static_cast<UINT32>(memval)
           << std::endl;
        pc += 1;
    }
    os.flags(fmt);
    
    return os.str();
}

/* ===================================================================== */

static VOID AtBranch(ADDRINT ip, ADDRINT target, BOOL taken, THREADID threadid)
{
	if (taken)
    {
        string s = disassemble ((ip),(ip)+15);
        ThreadData* tdata = get_tls(threadid);
        fprintf (tdata->Trace_File, "%s\n", s.c_str()); //prints: pc, assembly
        fflush (tdata->Trace_File);
        PIN_LockClient();
        CheckIfUnique(s);
        PIN_UnlockClient();
    }
} 

/* ===================================================================== */
static VOID Trace(TRACE trace, VOID *v)
{
	UINT64 trace_addr = TRACE_Address(trace);
	IMG img = IMG_FindByAddress(trace_addr);
	if(!IMG_Valid(img)) return;
	ADDRINT version = TRACE_Version(trace);

	UINT64 img_low_addr = IMG_LowAddress(img);	
	UINT64 trace_rel_addr = trace_addr - img_low_addr;
	USIZE trace_size = TRACE_Size(trace);
	THREADID thread_id = PIN_ThreadId();
	bool Allow_Instrum = 1;
	if(Redun_Suppress)
	{
		Allow_Instrum = dime_compare_to_log(thread_id, trace_rel_addr, trace_size);
	}
	
	if(Allow_Instrum)
	{
		for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl))
		{
			for (INS ins = BBL_InsHead(bbl); INS_Valid(ins); ins = INS_Next(ins))
			{
				if (INS_IsBranchOrCall(ins))
				{
					dime_switch_version(version, ins);
					switch(version) {
					  case VERSION_BASE:
					  	//Do Nothing 
						break;
					  case VERSION_INSTRUMENT:
						INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)AtBranch, IARG_INST_PTR, 
						IARG_BRANCH_TARGET_ADDR, IARG_BRANCH_TAKEN, IARG_THREAD_ID, IARG_END);
						break;
					  default:
						assert(0); 
						break;
					}
				
				}
			}
		}		
		dime_modify_log(version, thread_id, trace_rel_addr, trace_size);
	}        
}
/* ===================================================================== */
VOID ThreadStart(THREADID threadid, CONTEXT *ctxt, INT32 flags, VOID *v)
{
	dime_thread_start(threadid);
	ThreadData* tdata = get_tls(threadid);
	ostringstream os;
	string file_name = File_Name;
	os << (threadid+1);
	file_name += os.str();//thread id
	file_name += ".out";
	tdata->Trace_File = fopen(file_name.c_str(),"w");
}
/* ===================================================================== */

VOID Fini(INT32 code, VOID *v)
{
//	for(int i = 0; i < indx; i++)
//	{
//		Trace_File2 << test[i] << endl;
//	}
	dime_fini();
	//Close Trace_File of each thread
	ThreadData* tdata;
	for(int t = 0; t < Num_Threads; t++)
	{
	 	tdata = get_tls(t);
	 	fclose(tdata->Trace_File);
	}
	//print timestamps of unique traces
	FILE* tm_file = fopen("branch_qdime_tm.txt", "w");
	for ( auto it = Time_Set.begin(); it != Time_Set.end(); ++it )
	{
		fprintf(tm_file, "%.9Lf\n", *it);
	}
	fclose(tm_file);
}

/* ===================================================================== */

int main(INT32 argc, CHAR **argv)
{
    PIN_Init(argc, argv);
    dime_init((char*)"branch_qdime_extra.out"); 	
	PIN_AddThreadStartFunction(ThreadStart, 0);
    TRACE_AddInstrumentFunction(Trace, 0);
	PIN_AddFiniFunction(Fini, 0);
    PIN_StartProgram();
    return 0;
}
